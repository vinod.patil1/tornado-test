from sqlalchemy import Column, String, DateTime
from sqlalchemy_utils import UUIDType

from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()


class Task(Base):

    __tablename__ = 'task'
    uuid = Column(UUIDType(binary=False), primary_key=True, nullable=False)
    name = Column(String)
    note = Column(String)
    creator = Column(String)
    creation_date = Column(DateTime)
