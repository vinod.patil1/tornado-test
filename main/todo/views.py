import json
import uuid
from datetime import datetime

from tornado.gen import coroutine
from tornado.web import RequestHandler
from tornado_sqlalchemy import SessionMixin

from main.db_utility import DBUtility
from main.todo.models import Task


class BaseView(RequestHandler, SessionMixin):
    """Base view for this application."""

    def prepare(self):
        self.form_data = {
            key: [val.decode('utf8') for val in val_list]
            for key, val_list in self.request.arguments.items()
        }

    def set_default_headers(self):
        """Set the default response header to be JSON."""
        self.set_header("Content-Type", 'application/json; charset="utf-8"')

    def send_response(self, data, status=200):
        """Construct and send a JSON response with appropriate status code."""
        self.set_status(status)
        self.write(json.dumps(data))


class HelloWorld(RequestHandler):
    """Print 'Hello, world!' as the response body."""

    def get(self):
        """Handle a GET request for saying Hello World!."""
        self.write("Hello, world!")


class TaskCreateListView(BaseView):
    """View for reading and adding new tasks."""
    SUPPORTED_METHODS = ("GET", "POST",)

    @coroutine
    def get(self):
        """Create a new task."""
        session = DBUtility()
        tasks = session.query(Task).all()
        response = []
        for task in tasks:
            response.append({'id':str(task.uuid), 'name': task.name, 'note': task.note})
        self.send_response(response, status=200)

    @coroutine
    def post(self):
        """Create a new task."""
        session = DBUtility()
        task = Task(
            uuid=uuid.uuid4(),
            name=self.form_data['name'][0],
            note=self.form_data['note'][0],
            creation_date=datetime.now(),
        )
        session.add(task)
        session.commit()
        session.flush()
        self.send_response({'msg': 'posted'}, status=201)


class TaskRetrieveUpdateView(BaseView):
    """View for reading and adding new tasks."""
    SUPPORTED_METHODS = ("GET", "POST", "DELETE")

    @coroutine
    def get(self, task_id):
        """Create a new task."""
        session = DBUtility()
        task = session.query(Task).filter(Task.uuid == task_id).first()
        self.send_response({'id':str(task.uuid), 'name': task.name, 'note': task.note}, status=200)

    @coroutine
    def delete(self, task_id):
        """Create a new task."""
        session = DBUtility()
        task = session.query(Task).filter(Task.uuid == task_id).delete()
        session.commit()
        self.send_response(status=200)
