from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application


#define('port', default=8888, help='port to listen on')
from main.relations.views import CollegeCreateListView, StudentCreateListView, CourseCreateListView, \
    CourseStudentListView
from main.todo.views import HelloWorld, TaskCreateListView, TaskRetrieveUpdateView


def main():
    """Construct and serve the tornado application."""
    app = Application([
        ('/', HelloWorld),
        ('/task', TaskCreateListView),
        ('/task/(.*)', TaskRetrieveUpdateView),
        ('/college', CollegeCreateListView),
        ('/college/(.*)/student', StudentCreateListView),
        ('/college/(.*)/course', CourseCreateListView),
        ('/course/(.*)/student', CourseStudentListView),

    ])
    http_server = HTTPServer(app)
    http_server.listen(5000)
    print('Listening on http://localhost:%i' % 5000)
    IOLoop.current().start()


if __name__ == "__main__":
    main()
