from tornado_sqlalchemy import make_session_factory

class DBUtility:

    session = None

    def __new__(self):

        DB_URL = 'postgres://user:password@postgres:5432/tornado_test'

        if DBUtility.session is None:
            factory = make_session_factory(DB_URL)
            DBUtility.session = factory.make_session()
            return DBUtility.session
        else:
            return DBUtility.session
