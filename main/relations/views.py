import json

from jsonschema import validate
from tornado.gen import coroutine
from tornado.web import RequestHandler
from tornado_sqlalchemy import SessionMixin

from main.db_utility import DBUtility
from main.relations.models import College, Student, Course


class BaseView(RequestHandler, SessionMixin):
    """Base view for this application."""

    def prepare(self):
        self.form_data = {
            key: [val.decode('utf8') for val in val_list]
            for key, val_list in self.request.arguments.items()
        }

    def set_default_headers(self):
        """Set the default response header to be JSON."""
        self.set_header("Content-Type", 'application/json; charset="utf-8"')

    def send_response(self, data, status=200):
        """Construct and send a JSON response with appropriate status code."""
        self.set_status(status)
        self.write(json.dumps(data))


class CollegeCreateListView(BaseView):
    """View for reading and adding new tasks."""
    SUPPORTED_METHODS = ("GET", "POST",)

    @coroutine
    def get(self):
        """Create a new task."""
        session = DBUtility()
        collegees = session.query(College).all()
        response = []
        for college in collegees:
            response.append({'id':college.id, 'name': college.name})
        self.send_response(response, status=200)

    @coroutine
    def post(self):
        """Create a new task."""
        session = DBUtility()
        college = College(
            name=self.form_data['name'][0],
        )
        session.add(college)
        session.commit()
        session.flush()
        self.send_response({'response': 'created'}, status=201)


class StudentCreateListView(BaseView):
    """View for reading and adding new tasks."""
    SUPPORTED_METHODS = ("GET", "POST",)

    @coroutine
    def get(self, college_id):
        """Create a new task."""
        session = DBUtility()
        students = session.query(Student).filter(Student.college_id == college_id).all()
        response = []
        for student in students:
            response.append({'id':student.id, 'name': student.name})
        self.send_response(response, status=200)

    @coroutine
    def post(self, college_id):
        """Create a new task."""
        session = DBUtility()

        college = session.query(College).filter(College.id == college_id).first()
        if not college:
            self.send_response({'response': 'college not found'}, status=404)
            return

        student = Student(
            name=self.form_data['name'][0],
            college_id=college_id
        )
        session.add(student)
        session.commit()
        session.flush()
        self.send_response({'response': 'created'}, status=201)


class CourseCreateListView(BaseView):
    """View for reading and adding new tasks."""
    SUPPORTED_METHODS = ("GET", "POST",)

    @coroutine
    def get(self, college_id):
        """Create a new task."""
        session = DBUtility()
        courses = session.query(Course).filter(Course.college_id == college_id).all()
        response = []
        for course in courses:
            response.append({'id':course.id, 'name': course.name})
        self.send_response(response, status=200)

    @coroutine
    def post(self, college_id):
        """Create a new task."""
        session = DBUtility()

        college = session.query(College).filter(College.id == college_id).first()
        if not college:
            self.send_response({'response': 'college not found'}, status=404)
            return

        course = Course(
            name=self.form_data['name'][0],
            college_id=college_id
        )
        session.add(course)
        session.commit()
        session.flush()
        self.send_response({'response': 'created'}, status=201)


class CourseStudentListView(BaseView):
    """View for reading and adding new tasks."""
    SUPPORTED_METHODS = ("GET", "POST",)

    @coroutine
    def post(self, course_id):
        """Create a new task."""
        session = DBUtility()
        try:
            body = json.loads(self.request.body)
            validate(instance=body, schema={"type": "array", "items": { "type": "integer"}})
        except Exception as ve:
            self.send_response({'response': 'Send a valid data', 'message': ve.message}, status=400)
            return

        course = session.query(Course).filter(Course.id == course_id).first()
        if not course:
            self.send_response({'response': 'course not found'}, status=404)
            return

        for student_id in body:
            student = session.query(Student).filter(Student.id == student_id).first()
            if student is not None:
                course.course_students.append(student)
        session.add(course)
        session.commit()
        session.flush()
        self.send_response({'response': 'created'}, status=201)
