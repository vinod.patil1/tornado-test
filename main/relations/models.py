from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship

from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()


class College(Base):
    __tablename__ = 'college'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    student = relationship("Student", back_populates="college")


class Student(Base):
    __tablename__ = 'student'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    college_id = Column(Integer, ForeignKey('college.id'))
    college = relationship("College", back_populates="student")
    # profile = relationship("StudentProfile", uselist=False, back_populates="student")


class StudentProfile(Base):
    __tablename__ = 'student_profile'
    student_id = Column(Integer, ForeignKey('student.id'), primary_key=True)
    age = Column(Integer)

student_courses_table = Table('student_courses', Base.metadata,
    Column('student_id', Integer, ForeignKey('student.id')),
    Column('course_id', Integer, ForeignKey('course.id'))
)

class Course(Base):
    __tablename__ = 'course'
    id = Column(Integer, primary_key=True)
    college_id = Column(Integer, ForeignKey('college.id'))
    name = Column(String)
    course_students = relationship("Student", secondary=student_courses_table)
